# Docker Architecture

Docker is an `OS Level Virtualizer` so for these items:

1. `Process & Scheduler`
2. `Network Interface`
3. `Inter Process Communication`
4. `Mountpoint & Filesystem`
5. `Timing`

It will use `Host OS Kernel` without any `Guest OS`

So we must **`Map`** (`Namespace`) our `Host OS Resources` to our `Container`

---

## Namespaces

Docker uses a technology called `namespaces` to provide the isolated workspace called the `container`.
When you run a container, Docker creates a set of namespaces for that container.

These namespaces provide a layer of isolation. Each aspect of a container runs in a separate namespace and its access is limited to that namespace.

Docker Engine uses namespaces such as the following on Linux:

1. The `pid` namespace: Process isolation (PID: Process ID).
2. The `net` namespace: Managing network interfaces (NET: Networking).
3. The `ipc` namespace: Managing access to IPC resources (IPC: InterProcess Communication).
4. The `mnt` namespace: Managing filesystem mount points (MNT: Mount).
5. The `uts` namespace: Isolating kernel and version identifiers. (UTS: Unix Timesharing System).

---

### Process ID Isolation (PID namespace)

Container Process ID's will start from `1` but the will run using `Host OS Kernel`, so using a `Map Function` they will map to our `Host OS` Process ID's

![PID Namespace](assets/pid_namespace.png)

---

### Filesystem Mountpoint Isolation (MNT namespace)

Container Filesystems must start from `Root Point` (`/`) but they will keep in our `Host OS Filesystem` so using a `Virtual Volume`, they mount point root exists on our `Host OS Path` (`/etc/docker/`)

---

### Network Interface Isolation (NET namespace)

Container Network interfaces must have a `eth0` interface but infact they will map to our `Host OS Interface` using `Bridge Interface` (`Map`)

---

## Control Groups

Every container can set some limits to `CPU`, `RAM`, `Hardware` usage using `cgroups`

![CGroups](assets/cgroups.png)

---
