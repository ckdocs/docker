# Basics

> Docker is a `Container Management Service` for `Develop`, `Ship`, `Run` any application on any platform without dependency problems

## Virtualization

There are two main types of virtualizations

1. **Virtual-Machine** base virtualization - `Hardware level`
2. **Container** base virtualization - `OS level`

![VM vs Container](assets/vm_vs_container.jpg)

### Virtual-Machine base virtualization (Hardware-Level)

Virtualizing in `Hardware level`

In this type of virtualization we have a virtualized hardware, then we will install a `Guest OS` on that, in fact over the `Host OS`, will a lot of `overhead` of hardware usage

#### Infrastructure

The bare metal server `hardware` that used as infrastructure of virtualization

---

#### Host OS

The `lightweight OS` that will only start the `Hypervisor` process for virtualization management

Examples:

1. Windows
2. Linux
3. Mac
4. BSD
5. ESXi

---

#### Hypervisor (Virtual Machine Monitor (VMM))

Hypervisor or the virtualization management application that will virtualize `Guest OS'es` (On virtual `Hardware`) over the `Host OS` and managing and scheduling `RAM`, `CPU`, etc between Virtual Machines.

Examples:

1. `VMware`:
    1. **Host OS**: Windows, Linux, Mac
    2. **Hypervisor**: `VMware`
2. `ESXi`:
    1. **Host OS**: Unix
    2. **Hypervisor**: `VMware ESXi`
3. `QEMU`:
    1. **Host OS**: Linux
    2. **Hypervisor**: `qemu - kqemu`
4. `Virtual Box`:
    1. **Host OS**: Windows, Linux, Mac
    2. **Hypervisor**: `Oracle Virtual Box`
5. `Hyper-V`
    1. **Host OS**: Windows
    2. **Hypervisor**: `Windows Hyper-V`

---

#### Cloud Provider (IaaS - Infrastructure as a Service)

There are some cloud providers that will provide a `Infrastructure` or `Virtualized Hardware` or `Virtual Machine` as a service

These type of service called `Infrastructure` or `Virtualized Hardware`, VPS'es are a type of `IaaS`, a virtualized hardware

Example:

1. [http://xaas.ir](http://xaas.ir)
2. [http://berbidvps.ir](http://berbidvps.ir)

---

### Container base virtualization (OS-Level)

Virtualizing in `OS level`

In this type of virtualization we have virtualized os, then we will run our `Application Instance` on that, in fact over the `Host OS`, with a few `overhead` of hardware usage

#### Platform

The `OS` that used as **HostOS** of virtualization

---

#### Host OS

The `lightweight OS` that will only start the `Container Engine` process for virtualization management

Examples:

1. Windows
2. Linux
3. Mac
4. BSD
5. CoreOS
6. RancherOS

---

#### Container Engine

Will virtualize `Applications` (On virtual `OS`) over the `Host OS` and managing and scheduling `RAM`, `CPU`, etc between Containers.

Examples:

1. `Docker`:
    1. **Host OS**: Windows, Linux, Mac, BSD, `CoreOS`, `RancherOS`
    2. **Container Engine**: `Docker daemon (dockerd)`
2. `LXC`:
    1. **Host OS**: Linux
    2. **Container Engine**: `LXC`
3. `OpenVZ`:
    1. **Host OS**: Linux
    2. **Container Engine**: `OpenVZ`
4. `Jail`:
    1. **Host OS**: FreeBSD
    2. **Container Engine**: `FreeBSD Jail`

---

#### Cloud Provider (PaaS - Platform as a Service)

There are some cloud providers that will provide a `Platform` or `Virtualized OS` or `Container` as a service

These type of service called `Platform` or `Virtualized OS`, Docker service's are a type of `PaaS`, a virtualized os

Example:

1. [http://fandogh.ir](http://fandogh.ir)
2. [http://liara.ir](http://liara.ir)
3. [http://heroku.com](http://heroku.com)

---

## Docker Ecosystem (Docker Platform/Engine)

![Docker Ecosystem](assets/docker_architecture.png)

Docker was built on top of `Client-Server` architecture

The entire system of docker called `Docker Engine`:

1. Docker CLI (Client)
2. Docker REST API (Server-API)
3. Docker Daemon (Server)
4. Docker Hub (Registry)
5. Docker Compose (Swarm Orchestration Config)
6. Docker Machine (?)

![Docker Ecosystem](assets/docker_ecosystem.png)

### Docker Server (Host) (Daemon)

1. `Container` management
2. `Image` management
3. `REST` API
4. Docker daemon (`dockerd`)

Docker server will `create`, `delete`, `start`, `stop` containers or `push`, `pull`, `build`, `commit` images to/from `Docker Machines (Docker Cluster/Nodes)`

Docker server has a `REST API` for integrating clients and commanding

Docker server has a core process named **Docker Daemon Service** (`dockerd`)

---

### Docker Client (CLI)

1. `Command` docker server

Docker client will send commands for `create`, `delete`, `start`, `stop` containers or etc to docker server

---

### Docker Registry (Hub)

Is a `public/private` image management repository (`git` for docker images)

---

### Example

1. We run `docker run hello-world` using **Docker CLI**
2. Query will send to **Docker Server** from **Docker CLI**
3. **Docker Server** will check **Local Image Cache** for image `hello-world`
4. **Docker Server** will pull image from **Docker Hub** into **Local Image Cache**
5. **Docker Server** will create new **Container** from the **Image**
6. **Docker Server** will start that **Container**

![Docker Run 1](assets/docker_run_1.png)
![Docker Run 2](assets/docker_run_2.png)

#### Tip (Local Image Cache)

If **Docker Server** find the `image` in **Local Cache** won't pull it:

First:

![Docker Pull Cache 1](assets/docker_pull_cache_1.png)

Second:

![Docker Pull Cache 2](assets/docker_pull_cache_2.png)

---

## Docker Architecture (Linux Kernel Feature)

For containerizing our applications, docker will use two `Kernel Features` named **Namespaces** and **Control Groups**

**Comprehension the problem**:

`Problem`: We have two programs `Chrome` (needs Python v2), `NodeJS` (needs Python v3), there's a conflict between our python versions

![Docker Architecture Problem](assets/docker_architecture_problem_1.png)

`Answer`: We can use the **namespaces** linux kernel feature to `Split Hardware Resources` into `Segments`:

![Docker Architecture Problem](assets/docker_architecture_problem_2.png)

In this example kernel will check:

-   If `chrome` will request python `=>` redirect to `segment 1`
-   If `nodejs` will request python `=>` redirect to `segment 2`

### Namespace (namespace)

![Namespaces](assets/namespaces.png)

A `Linux` kernel feature for isolating one or more processes in these items:

1. Processes (PSTree)
2. Hard Drive (HDD)
3. Network (Interfaces)
4. Users (Users, Groups)
5. Hostnames
6. IPC

> Every isolated group called `Container`(Docker) or `POD`(Kubernetes)

#### Example

For the previews problem our answer using namespaces is:

![Namespaces Example](assets/namespaces_example.png)

---

### Control Group (cgroup)

![Control Groups](assets/controlgroups.png)

A `Linux` kernel feature for limiting access of `namespaces` on `CPU`, `RAM`, `IO speed`, `Network speed`

#### Example

![Control Groups Example](assets/controlgroups_example.png)

---

## Docker Image/Container

![Docker Image Container](assets/docker_image_container.png)

### Docker Image

A full snapshot of our application and it's dependencies that packed together into an image file, ready to create the startable container

![Docker Image](assets/docker_image.png)

### Docker Container

A copy of image into `Container ID Folder` that will ready to start

![Docker Container](assets/docker_container_1.png)

#### docker container create

![Docker Container](assets/docker_container_2.png)

#### docker container start

![Docker Container](assets/docker_container_3.png)

---

## Installation

See this [link](https://docs.docker.com/install/) to begin how to install docker

Docker uses `Namespaces` and `Control Groups`, the are only available on `Linux`, so for using Docker in `Windows` or `Mac`:

1. Install Hardware Virtualizer Tool (HyperV - VirtualBox - ...)
2. Install Virtual Linux OS
3. Install Docker Server on virtualized linux
4. Install Docker Client on your OS
5. Connect Docker Client to Virtualized Docker Server

### Docker Toolbox (Legacy) (VirtualBox)

Legacy version of docker for `Windows`, `Mac`:

1. Install VirtualBox
2. Install Linux VM
3. Install Docker Server Linux
4. Install Docker Client Windows/Mac

---

### Docker for Mac (KVM)

1. Install KVM (`New`)
2. Install Linux VM
3. Install Docker Server Linux
4. Install Docker Client Mac

![Docker for mac](assets/docker_for_mac.png)

---

### Docker for Windows (Windows 10) (Hyper-V)

1. Install/Enable Hyper-V (`New`)
2. Install Linux VM (Called `MobyLinuxVM` on `Hyper-V`)
3. Install Docker Server Linux
4. Install Docker Client Mac

![Docker for windows](assets/docker_for_windows.png)

#### Tip

This version of docker for windows will support two types of containers:

1. Linux Container (Use `Linux VM`)
2. Windows Container (Use `Hyper-V Windows Virtualizer`)

##### Linux Container (Default) (use Linux VM)

The default option for container types is `Linux Containers`

There are many options:

1. `ubuntu` image
2. `centos` image
3. etc

##### Windows Container (use Hyper-V)

The second option for container types is `Windows Containers` for whenever you must use a `windows` as base image for your application

There are many options:

1. `Windows Server Core` image
2. `Nano Server` image

---
