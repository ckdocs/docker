# Docker Network

Docker network is the most important module in the docker engine used for connecting containers to each other and securing and managing their connections

Docker network usages:

-   Connecting containers
-   Securing/Managing containers network
-   Load balancing
-   Service discovery

<!-- prettier-ignore -->
!!! tip "Docker vs VMware"
    Docker has it's own concepts that against VMware concepts
    ![Docker Network](assets/docker_network_vs.png)

Docker network has three basis pillars:

1. **CNM** (Container Network Model)
2. **Libnetwork**
3. **Drivers**

## Pillars

There are three main concepts created the docker network:

![Docker Network Pillars](assets/docker_network_1.png)
![Docker Network Pillars](assets/docker_network.png)

### CNM (Container Network Model)

Is only a concept, this concepts says: every network can create using three basic components:

![CNM](assets/network_cnm.png)

1. **Sandbox**: Is the isolation of networks (`VLAN's`)
2. **Endpoint**: Is the interfaces on network `eth0`, `docker0`, etc
3. **Network**: Connected endpoints in one or more sandboxes called a network

![CNM](assets/network_cnm_detail.png)

#### CNM (Docker) vs CNI (Kubernetes)

Like docker, the Kubernetes also has these concepts, that called `CNI`

![CNM vs CNI](assets/network_cnm_vs_cni.png)

---

### Libnetwork

Is the implementation of `CNM` in `Golang` used by docker engine

It has core concepts: `Sandbox`, `Endpoint`, `Network`

---

### Drivers

The higher level implementation of networks called `Drivers`

There are two types of drivers:

1. **Native Drivers**: Are provided by libnetwork natively
    1. `bridge` - `nat`
    2. `overlay` - none
    3. `macvlan` - `l2bridge`
    4. `ipvlan` - `l2bridge`
2. **Remote Drivers**: Implemented by other developers
    1. `contiv`
    2. `kuryr`
    3. etc

There are many concepts in drivers:

1. Scope
2. Driver
3. IPAM (IP Address Management)
    1. Driver
    2. Config
4. Default Bridge

#### Scope

Every driver has an scope, there are two scopes:

1. **local**: Single host
2. **swarm**: Multi host (Distributed - Cluster)

---

#### Driver

There are many types of drivers:

1. **bridge**
2. **overlay**
3. **host**
4. **macvlan**
5. **ipvlan**

<!-- prettier-ignore -->
!!! tip "Docker Network Driver"
    Using `docker info | grep Network` command you can get the **list** of available drivers on your system

---

#### IPAM (IP Address Management)

Is a module for managing clients on the network and has it's own DHCP server

<!-- prettier-ignore -->
!!! tip "Docker Network IPAM"
    IPAM has it's own **drivers** and we can change it's default driver

---

#### Default Bridge

Every driver has a default bridge driver mapped to host driver, the default default driver is `docker0`

---

![Docker Network Pillars](assets/docker_network_2.png)

---

## Drivers Types

There are three main native categories for drivers

1. **Single Host**
2. **Multi Host**
3. **Existing Host**

### Single Host

Single host means, the network is only available on **Current Host**

![Single Host](assets/network_single_host.png)

And the containers on other hosts cannot contact with each other

---

#### Bridge - NAT

Is `Single Host networking` driver, is an alternative to a `Virtual Switch` in docker

-   **Linux**: `bridge`
-   **Windows**: `nat`

![Bridge](assets/network_bridge.png)

```bash
docker network create -d bridge --subnet 172.30.0.0/24 my-bridge
docker run ... --network my-bridge
```

---

### Multi Host

Multi host means, the network is available on **Multi Host (Cluster)**

![Multi Host](assets/network_multi_host.png)

And the containers on other hosts now can contact with each other using **Swarm Orchestration**

---

#### Overlay

Is `Multi Host networking` driver, is an alternative to a `XLAN` in docker named `VXLAN`

-   **Linux**: `overlay`

![Overlay](assets/network_overlay.png)

```bash
docker swarm init
docker swarm join ...

docker network create -d overlay --subnet 10.5.0.0/24 my-overlay
docker run ... --network my-overlay --replicas 4
```

---

### Existing Host

Existing host means, the network will use the existing **Physical Networks** and will get an **IP** from **DHCP Server** on the physical network

![Existing Host](assets/network_existing_host.png)

And the containers on other hosts cannot contact with each other

---

#### MACVLAN - L2Bridge (For Bare Metal Servers)

Is `Existing Host networking` driver, where docker containers will get's a real **MAC** address and **IP** on the existing **VLAN** network

-   **Linux**: `macvlan`
    1. Every container get's it's own **IP**
    2. Every container get's it's own **MAC**
-   **Windows**: `l2bridge`
    1. Every container get's it's own **IP**
    2. All containers share a common **MAC**

![MACVLAN](assets/network_macvlan.png)

```bash
docker network create -d macvlan --subnet 192.168.0.0/24 --gateway 192.168.0.1 my-vlan
docker run ... --network my-vlan
```

---

#### IPVLAN - L2Bridge (For Cloud Providers)

Is `Existing Host networking` driver, where docker containers will get's the host **MAC** address and real **IP** on the existing **VLAN** network

-   **Linux**: `ipvlan`
    1. Every container get's it's own **IP**
    2. Every container get's it's host **MAC**
-   **Windows**: `l2bridge`
    1. Every container get's it's own **IP**
    2. All containers share a common **MAC**

![IPVLAN](assets/network_ipvlan.png)

```bash
docker network create -d ipvlan --subnet 192.168.0.0/24 --gateway 192.168.0.1 my-vlan
docker run ... --network my-vlan
```

---

## DNS Service

Every **container** or **service (replicated containers)** that created in a network will accessable by it's `Name` or `Alias` (`--name myContainer` or `--alias myContainer`)

<!-- prettier-ignore -->
!!! tip "Docker DNS Service"
    Every container uses the host DNS service created by **Docker Daemon** listening on `127.0.0.11:53`

### Container DNS

Containers on the same network can access each other using their **name** as **hostname**

![Container DNS](assets/network_container_dns.png)

```bash
docker run ... --name c1
docker run ... --name c2

docker exec -it c1 bash
ping c2
```

---

### Service DNS

Every service created by docker has a **VIP** or **Virtual IP**, when we request that ip our request will redirect to one of the replicas of that service using a **Load Balancer**

![Service DNS](assets/network_service_dns.png)

---

## Advance Commands

### Creating/Removing

1. `docker network create`: create new network
2. `docker network rm`: remove a network
3. `docker network prune`: remove all unused networks

#### Creating flags

1. `--driver {driver}, -d {driver}`: set network driver

---

### Connect/Disconnect

1. `docker network connect`: connect a container to a network
2. `docker network disconnect`: disconnect a container from a network

---

### Status

1. `docker network ls`: list all networks
2. `docker network inspect`: show more info of a network

---
