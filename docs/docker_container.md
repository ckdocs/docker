# Docker Container

## Steps to start a container

There are three steps to start a `container` from an `image`:

### Pull image from registry

First docker server will check the `Local Image Cache`, if it finds the image will skip pulling it else it will pull the image from `Docker Hub (Registry)`

![Image to Container](assets/image_to_container_1.png)

> If image will find in the `Local Image Cache` it won't re-pull again

---

### Create container from image

Now docker server will copy the `Image Data` into new folder with name of `Container ID` and init the container `Network Interfaces` or `Volumes`

![Image to Container](assets/image_to_container_2.png)

#### Container Isolation

Every container has it's own filesystem so our containers are isolated

```code
docker container run busybox => {id=1}
docker container run busybox => {id=2}

docker container exec 1 touch a => {create in id=1 container folder}
docker container exec 2 cat a => {doesn't exists }
```

---

### Start the container

At the end, docker server will run the `Startup Command` of image to start the created container from `/docker/{container_id}/app` container folder

![Image to Container](assets/image_to_container_3.png)

#### Default Command (Startup Command)

Every image can have a `Default Command`, that when we want to start the container that command will run to start the application, but some images hasn't any default command (like `busybox`)

Also when we want to run the container we can override our custom default command:

##### Example

```code
docker run {image} [custom default command]

docker run hello-world
docker run busybox echo hello world
```

Running `docker run busybox ls` using custom default command (`ls`) in busybox image:

![Default Command](assets/default_command_1.png)
![Default Command](assets/default_command_2.png)

---

## Container Lifecycle

Docker containers have a `FSM` of states and they can change their own state using `Docker CLI` commands

![Docker Commands](assets/docker_commands_3.png)
![Docker Container FSM](assets/docker_container_fsm.jpeg)

### Container States

#### Null

When the container fs doesn't exists on HDD (doesn't copied from image data)

![Container Lifecycle](assets/container_lifecycle_1.png)

#### Created

When we copy the image `FS Snapshot` to the `Container ID` folder

![Container Lifecycle](assets/container_lifecycle_2.png)

#### Running

When we run the `Default Command` of image in the `Container ID` folder

![Container Lifecycle](assets/container_lifecycle_3.png)

#### Pause

When container `Processes` paused from `Kernel Scheduling`

---

## Basic Commands

![Docker Commands](assets/docker_commands_1.png)
![Docker Commands](assets/docker_commands_2.png)

---

## Advance Commands

### Creating/Removing/Save

1. `docker container create`: create container folder
2. `docker container rename`: change container name
3. `docker container cp`: copy file between machine and container
4. `docker container rm`: remove container folder
5. `docker container prune`: remove all stopped containers
6. `docker container commit`: commit container folder changes to image
7. `docker container export`: get `.tar` file

#### Creating flags

1. `--name`: container name

---

### Starting/Stopping

1. `docker container run`: create + start
2. `docker container start`: run default command from image in container folder
3. `docker container restart`: stop + start
4. `docker container stop`: stop container processes => (`SIGTERM`)
5. `docker container kill`: force stop container procceses => (`SIGKILL`)
6. `docker container wait`: wait until stopping the container and get exit code

#### Starting flags

1. `-a`: attach => don't start in background
2. `-d`: daemon => run in the background
3. `-i`: interactive => connect `STDIO` to container run in background (`-it`)
4. `-t`: terminal => beautiful show `STDIO` (`-it`)
5. `-p hostIP:hostPort:containerPort`: port => map a host port to a container port
6. `-v hostVolume:containerVolume`: volume => map a host volume to a container volume
7. `-v containerPath`: exclude a path in container to link volume
8. `--network driverName`: add container to existing network

**Example**:

```bash
docker run -p 3001:3001 -v /app/node_modules -v $(pwd):/app <image_id>
```

---

### Pause/Unpasue

1. `docker container pause`: pause container processes
2. `docker container unpause`: resume container processes

---

### Status

1. `docker container ls`: list runnung/all containers
2. `docker container logs`: show logs of a container
3. `docker container inspect`: show more info of a contaier (ip, port, configs, ...)
4. `docker container port`: show listening ports of a container
5. `docker container stats`: show status of a container
6. `docker container top`: show processes of a container

#### List flags

1. `--all`: list all containers (stopped, running, paused)

---

### Remote

1. `docker container exec`: run command in container
2. `docker container attach`: connect to the `Default Command` stdio of running container

![Container Command](assets/container_command_exec.png)

```code
docker container exec {id} -it echo hi there
docker container exec {id} -it bash
```

---
