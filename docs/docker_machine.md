# Docker Machine

Is another `CLI` named `docker-machine`, used to install `OS` and `Docker Host` and `SSH` on an infrastructure and creating a safe connection to that machine

![Docker Machine](assets/docker_machine.jpeg)

We can manage our docker hosts and connect our docker client to that hosts using this tool

First we must install `docker-machine` command, follow [this](https://github.com/docker/machine/releases) link

```bash
curl -L https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
    chmod +x /tmp/docker-machine &&
    sudo cp /tmp/docker-machine /usr/local/bin/docker-machine
```

---

## Drivers

We can create machines on various types of `Infrastructures` like **ESXI** or **VirtualBox** or **Bare Metal** or etc, using drivers we can interact with any types of infrastructures

There are many types of drivers for these infrastructures:

1. VMWare Fusion
2. VMWare VSphere
3. VMWare Workstation
4. Generic (Installed OS)
5. Amazon AWS
6. Digital Ocean
7. Google GCE
8. Microsoft Hyper-V
9. etc

![Docker Machine Drivers](assets/docker_machine_drivers.png)

---

### Example

1. Install **Boot2Docker OS**, **Docker Host**, **SSH** on vsphere machine:

```bash
export VSPHERE_CPU_COUNT=4
export VSPHERE_MEMORY_SIZE=12288
export VSPHERE_DISK_SIZE=307200
export VSPHERE_VCENTER="192.168.100.140"
export VSPHERE_USERNAME="root"
export VSPHERE_PASSWORD="password"
export VSPHERE_NETWORK="VM Traffic"
export VSPHERE_DATASTORE="datastore1"

docker-machine create -d vmwarevsphere Docker
docker-machine regenerate-certs Docker
```

2. Configure **Static IP**, **DNS** (Edit file `/var/lib/boot2docker/bootlocal.sh`):

```bash
# configure IP
kill `cat /var/run/udhcpc.eth0.pid`
ifconfig eth0 192.168.100.141 netmask 255.255.255.0 up
ip route add default via 192.168.100.1 dev eth0

# configure DNS
cat << EOF > /etc/resolv.conf
nameserver 185.55.226.26
nameserver 185.55.225.25
EOF
```

<!--prettier-ignore-->
!!! tip "Boot2Docker"
    Boot2Docker is a very **tiny OS** that only boots docker host

---

## Advance Commands

### Create/Remove/Save

1. `docker-machine create`: create machine
2. `docker-machine rm`: remove machine with data
3. `docker-machine upgrade`: upgrade machine OS

#### Example

```bash
export VSPHERE_CPU_COUNT=4
export VSPHERE_MEMORY_SIZE=12288
export VSPHERE_DISK_SIZE=307200
export VSPHERE_VCENTER="192.168.100.140"
export VSPHERE_USERNAME="root"
export VSPHERE_PASSWORD="password"
export VSPHERE_NETWORK="VM Traffic"
export VSPHERE_DATASTORE="datastore1"

docker-machine create -d vmwarevsphere Docker
```

---

### Starting/Stopping

1. `docker-machine start`: start machine OS
2. `docker-machine restart`: restart machine OS
3. `docker-machine stop`: stop machine OS
4. `docker-machine kill`: force stop machine OS

#### Example

```bash
docker-machine stop Docker

docker-machine start Docker
```

---

### Status

1. `docker-machine ls`: list running/all machines
2. `docker-machine active`: show which machine is active
3. `docker-machine config`: show connection info about a machine
4. `docker-machine inspect`: show more info about a machine (ip, cpu, ram, ...)
5. `docker-machine ip`: show ip of a machine
6. `docker-machine status`: show status of a machine
7. `docker-machine url`: show url of a machine

#### Example

```bash
docker-machine ls

docker-machins ip Docker
```

---

### Remote

1. `docker-machine env`: get env vars to switch docker client to a machine
2. `docker-machine ssh`: ssh to docker machine
3. `docker-machine scp`: copy file to/from docker machine
4. `docker-machine mount`: mount/unmount sshfs driver from machine
5. `docker-machine regenerate-certs`: regenerate machine connection certificates

#### Example

```bash
eval `docker-machine env Docker`

docker-machine ssh Docker
```

---
