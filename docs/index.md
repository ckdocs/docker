# Intro

Docker is a `Container Management Service` for `Develop`, `Ship`, `Run` any application on any platform without dependency problems

![Docker Architecture](assets/docker_architecture.png)

---
