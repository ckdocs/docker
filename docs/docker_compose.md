# Docker Compose

Is another `CLI` named `docker-compose`, used to start up multiple container and linking them together and simplifying the docker CLI arguments into one `docker-compose.yml` file

> Automates some of the long-winded arguments we were passing to `docker run ...`

We convert our `docker build ...` and `docker run ...` into a `.yml` format and save them into `docker-compose.yml` file

![Docker Compose](assets/docker_compose.png)

<!-- prettier-ignore -->
!!! info "Docker Compose Alternatives"
    There are many alternatives for `Docker Compose`, like `Kubernetes`, `Amazon AWS`, `Amazon ECS`, `Amazon Elastic Beanstalk`, Other `PaaS` cloud providers that support `Container Definition Files` or `Multi-Container Services`
    > For example, `Amazon Elasticbeanstalk` supports `Dockerrun.aws.json` file that is an alternative for `docker-compose.yml` file
!!! tip "Docker Compose Alternatives"
    1. **Docker Compose**: `VPS` - `Develop/Production`
    2. **Kubernetes/K8S**: `VPS/Provider` - `Develop/Production`
    3. **Amazon**: `Provider` - `Production`
!!! warning "Docker Compose Alternatives"
    Cloud providers, provides instances for `Database`, `Cache`, `FileStorage`, `Application` and we **must** use them for database/cache/storage because of lower cost and higher security and easier management (scalablity)

First we must install Docker Compose using command bellow:

```bash
sudo apt install docker-compose
```

## `docker-compose.yml`

The structure of this config file is like bellow sudo code:

```yml
version: "3"
services:
    serviceA:
        # configs
    serviceB:
        # configs
    serviceC:
        # configs
```

We have two main tags with name `version` and `services`, and in services we defined our `Containers` and their configurations

### Service(Container) Configs

#### Image/Build

At the first step for defining our container configs we must define the `image` we are using, we can use an `image name` or `building` our container from a `Dockerfile`

**Example**:

```yml
version: "3"
services:
    redis:
        image: "redis/latest"
    node-app:
        build: ./app
```

---

#### Networking

There are multiple configurations for networking in docker:

1. Port forwarding
2. Container linking

##### Port forwarding

Using `ports` config we can define our port mappings (`host_ip:host_port:container_port`)

```yml
version: "3"
services:
    redis:
        image: "redis/latest"
    node-app:
        build: ./app
        ports:
            - "4000:8000"
```

##### Container linking

Our containers are linked together by default, and we can access our containers from different container using `Service Name`

```yml
version: "3"
services:
    redis-server: # we can access `node-app` host name
        image: "redis/latest"
    node-app: # we can access `redis-server` host name
        build: ./app
        ports:
            - "host:container"
```

---

#### Restart Policy

Sometimes our container will exit with code `0` or error with code `> 0`, there are four policies for restarting our container automatically

![Docker Compose Restart Policies](assets/docker_compose_restart.png)

| Policy         | Exit Code |
| -------------- | --------- |
| always         | code >= 0 |
| on-failure     | code > 0  |
| unless-stopped | code = 0  |
| no             | -         |

Using `restart` config key we can sets a restart policy for a container

```yml
version: "3"
services:
    redis-server:
        image: "redis/latest"
    node-app:
        build: ./app
        restart: always
        ports:
            - "host:container"
```

---

### Example

Every application has two phases:

1. Development
    1. Watch
    2. Test
2. Production
    1. Build
    2. Run

#### Development

In development mode, we can use `npm run start` for running development watcher webserver

![Development Environment](assets/development_env.png)

**`Dockerfile.dev`**:

```dockerfile
FROM node:alpine

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .

CMD ["npm", "run", "start"]
```

**`docker-compose.dev.yml`**:

```yml
version: "3"
services:
    web:
        build:
            context: .
            dockerfile: Dockerfile.dev
        ports:
            - "3000:3000"
        volumes:
            - /app/node_modules
            - .:/app
    tests:
        build:
            context: .
            dockerfile: Dockerfile.dev
        ports:
            - "3000:3000"
        volumes:
            - /app/node_modules
            - .:/app
        command: ["npm", "run", "test"]
```

---

#### Production

In development mode, we must use a webserver like `Nginx` or `Apache`

![Production Environment](assets/production_env.png)

In production mode we have two phases:

1. Building
2. Running

So our `Dockerfile` has two phases

**`Dockerfile`**:

```dockerfile
# phase one: building
FROM node:alpine as builder
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

# phase two: running
FROM nginx
COPY --from=builder /app/build /usr/share/nginx/html
```

**`docker-compose.yml`**:

```yml
version: "3"
services:
    web:
        build:
            context: .
            dockerfile: Dockerfile
        ports:
            - "3000:3000"
```

---

## Advance Configs

The main structure of the `docker-compose.yml` is like this sudo code:

```yml
version: "3"
services:
    serviceA:
        # service configs
    serviceB:
        # service configs
volumes:
    volumeA:
        # volume configs
    volumeB:
        # volume configs
networks:
    networkA:
        # network configs
    networkB:
        # network configs
```

### Services

In every service section we describe our service (`Container`) configurations using commands bwllow

#### Building

We describe our container source image using `build` or `image` key

```yml
# build from Dockerfile
build: .

# build from custom Dockerfile
build:
    context: ./dir
    dockerfile: Dockerfile.dev

# build from image
image: ubuntu
image: ubuntu:14.04
image: tutum/influxdb
image: example-registry:4000/postgresql
image: a4bc65fd
```

**Docker equivalent commands**:

```bash
docker image build .
docker image build -f ./dir/Dockerfile.dev
docker image pull ubuntu
```

---

#### Restart Policies

We can describe our containers auto restart policy using `restart` key

```yml
restart: always

restart: on-failure

restart: unless-stopped

restart: no
```

---

#### Ports

We describe our containers port mapping using `ports` key

```yml
ports:
    - "3000" # shortcut for 3000:3000
    - "8000:80" # host:container
    - "172.30.0.1:81:80" # map 80 of container to 81 of 172.30.0.1 (bridge gateway)

# expose ports to linked services (not to host)
expose: ["3000"]
```

**Docker equivalent commands**:

```bash
docker run -p 3000 ...
docker run -p 3000:80 ...
docker run -p 172.30.0.1:81:80 ...
```

---

#### Volumes

We describe our containers volume (path) mapping using `volumes` key

```yml
volumes:
    - /var/lib/mysql # exclude (dont override using next maps)
    - ./_data:/var/lib/mysql
    - myVolume:/container/mount/path # we can import volume from a `volumes` section
```

**Docker equivalent commands**:

```bash
docker run -v /var/lib/mysql ...
docker run -v ./_data:/var/lib/mysql ...

docker volume create ...
docker run -v myVolume:/container/mount/path ...
```

---

#### Networks

We describe our containers networks connected using `networks` key

```yml
networks:
    backNetwork:
    dbNetwork:
```

**Docker equivalent commands**:

```bash
docker run --network backNetwork --network dbNetwork ...
```

---

#### Environments

We can describe our containers env variables using `environment`, `env_file` keys

```yml
# environment vars
environment:
    RACK_ENV: development
    HTTP_PORT: 4000

environment:
    - HTTP_HOST # value read from our host OS
    - RACK_ENV=development
    - HTTP_PORT=4000

# environment vars from file
env_file: .env
env_file: [.env, .development.env]
```

**Docker equivalent commands**:

```bash
docker run -e RACK_ENV=development -e HTTP_PORT=4000 ...
```

---

#### Commands

We can override our containers default command using `command` key

```yml
# command to execute
command: bundle exec thin -p 3000
command: [bundle, exec, thin, -p, 3000]
command: ["npm", "run", "test"]

# override the entrypoint
entrypoint: /app/start.sh
entrypoint: [php, -d, vendor/bin/phpunit]
```

**Docker equivalent commands**:

```bash
docker run ... bundle exec thin -p 3000
docker run ... npm run test
```

---

#### Dependencies

We can define containers starting order by defining their dependecies to each other using `links`, `external_links`, `depends_on` keys

```yml
links:
    - db:database # make sure `db` service is available before start and change it hostname to `database`
    - redis # make sure `redis` service is available

depends_on:
    - db # make sure `db` service is available

external_links:
    - redis_1 # make sure outside container named `redis_1` is available
    - project_db_1:mysql # make sure outside container named `project_db_1` is available and name it to `mysql`
```

---

#### Extending

We can import a service from a `docker-compose.yml` to another file using extending using `extends` key

```yml
extends:
    file: common.yml
    service: webapp
```

---

### Volumes

We can define volumes for share storage between containers

```yml
volumes:
    myVolume: /host/path/to/share
    hisVolume: /host/path/to/another/share
```

---

### Networks

We can define networks for containers to connect

```yml
networks:
    default:
        driver: bridge
        external: true
    backNetwork:
        driver: bridge
    dbNetwork:
        driver: bridge
```

---

## Advance Commands

> We must run all these commands on a folder containing our `docker-compose.yml` file

### Building/Pushing/Pulling

1. `docker-compose build`: building the all `Dockerfile's`
2. `docker-compose push`: pushing the images
3. `docker-compose pull`: pulling the images

---

### Creating/Removing

1. `docker-compose create`: creating all containers
2. `docker-compose rm`: removing all containers

---

### Starting/Stopping

1. `docker-compose start`: starting all containers
2. `docker-compose stop`: stopping all containers
3. `docker-compose kill`: killing all containers
4. `docker-compose restart`: restarting all containers
5. `docker-compose up`: create + start
6. `docker-compose down`: stop + rm

#### Flags

1. `-d`: run in daemon
2. `--build`: build the `Dockerfile's`

---

### Pause/Unpause

1. `docker-compose pause`: pausing all containers
2. `docker-compose unpause`: unpausing all containers

---

### Status

1. `docker-compose top`: containers top
2. `docker-compose ps`: containers ps
3. `docker-compose logs`: containers logs
4. `docker-compose events`: containers events
5. `docker-compose port`: containers public listening ports

---

### Scale

1. `docker-compose scale`

---
