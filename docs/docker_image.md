# Docker Image

## Dockerfile

Is a `Source` file that containes the steps for building an image, by building (`compile`) it using our docker, the builded image will add to our docker local image cache.

### Parts

Every `Dockerfile` has three main parts:

![Dockerfile Steps](assets/dockerfile_steps.png)
![Dockerfile Steps](assets/dockerfile_steps_code.png)

#### Base Image

At the first section we will set the base image using `FROM` keyword

> Modularity is an important tip for better life:
>
> For example we use <s>`ubuntu`</s> then install `nodejs`, but better way is to use <b>`node`</b> image
>
> > So we use the ready and builded images for our work

```dockerfile
FROM alpine:latest
```

---

#### Commands

The we use some commands to config image or install some dependencies or etc

```dockerfile
RUN apk add --upgrade redis
```

---

#### Default Command (Startup command)

At the final step we define the `Startup Command` of our image using `CMD` keyword

```dockerfile
CMD ["redis-server"]
```

---

### Layer

`Dockerfile` is layered, every `Line` in this file is a layer on the previews version

> For better performance on building, docker will cache all image layers for other images

---

## Steps to build an image

There are some steps to building an `Image` from a `Dockerfile`, or from a `Container`

### Dockerfile to Image (build)

Every line in `Dockerfile` called a `Layer`, every line will act as a layer on top of previews lines generated image

> By changing `Dockerfile` lines or their `Orders` the **`hash will change`** so the builder will build (not cache)

#### Algorithm

```js
// memoization imageCache :)
let imageCache = ...;

function build(Dockerfile) {
    let returnImage = undefined;

    for (let line in Dockerfile) {
        let imageHash = hash(lines[0:line]);

        // memoization images :)
        if (imageCache[imageHash]) {
            // this line image cached before
            return image = imageCache[imageHash];
        } else {
            // this line image not cached

            // create intermediate(temp) container from previews step image
            let container = new Container(returnImage);

            // run the line in container
            container.run(line);

            // save container as image
            returnImage = container.commit();

            // remove container
            container.remove();
        }
    }

    return returnImage;
}
```

---

#### Example

Let's interpret our example Dockerfile:

```dockerfile
FROM alpine
RUN apk add --update redis
CMD ["redis-server"]
```

And run `docker build .` in that directory:

---

##### `FROM alpine`

Image not found => `pull` image from `Registry`:

![Dockerfile Build](assets/docker_build_command_1.png)

**Image ID(Hash)**: `11cd0b38bc3c`

###### Read next line & Pull image

![Docker Build](assets/docker_build_1.png)

---

##### `RUN apk add --update redis`

Image not found => `intermediate container` building:

![Dockerfile Build](assets/docker_build_command_2.png)

**Intermedate Container ID(Hash)**: `30c5aa616f98` (Removed after build)

**Image ID(Hash)**: `38ec9aea7e10`

###### Read next line

![Docker Build](assets/docker_build_2.png)

###### Create container (Copy Filesystem)

![Docker Build](assets/docker_build_3.png)

###### Run command line

![Docker Build](assets/docker_build_4.png)

###### Save image & Remove container

![Docker Build](assets/docker_build_5.png)

---

##### `CMD ["redis-server"]`

Image not found => `intermediate container` building:

![Dockerfile Build](assets/docker_build_command_3.png)

**Intermedate Container ID(Hash)**: `e73a22decb35`

**Image ID(Hash)**: `fc60771eaa08`

###### Read next line

![Docker Build](assets/docker_build_6.png)

###### Create container & Run command line

![Docker Build](assets/docker_build_7.png)

###### Save image & Default command

![Docker Build](assets/docker_build_8.png)

---

### Container to Image (commit)

We can manually `Commit` a `Container` with all of it changes to `Image`:

![Docker Commit](assets/docker_commit.png)

```code
docker container commit -c 'CMD ["redis-server"]' {id}
```

---

## Basic Commands

![Docker Commands](assets/docker_commands.png)

---

## Advance Commands

### Building

1. `docker image build`: build the folder with `Dockerfile`
2. `docker image tag`: add tag to image
3. `docker builder prune`: remove build cache images

#### Building flags

1. `-t`: add tag
2. `-f`: set Dockerfile file path

![Docker Build Tag](assets/docker_build_tag.png)

---

### Status

1. `docker image ls`: list images
2. `docker image inspect`: get image info
3. `docker image history`: history of one image changes

---

### Registry/Hub (Push/Pull)

1. `docker image push`: push image to hub
2. `docker image pull`: pull image from hub
3. `docker search`: search image in hub

---

### `.tar` File (Save/Load)

1. `docker image save`: save .tar file
2. `docker image load`: load .tar file
3. `docker image import`: import .tar or stdin

---

### Removing

1. `docker image rm`: remove image
2. `docker image prune`: remove all unused images

---
