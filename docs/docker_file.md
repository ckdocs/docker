# Docker File

Is the source file of and `Image`, by build it instructions we get an image.

Every `Dockerfile` contains three main parts:

![Dockerfile Steps](assets/dockerfile_steps.png)
![Dockerfile Steps](assets/dockerfile_steps_code.png)

---

## Example

For example we have a `nodejs` application and we want to write a `Dockerfile` for it:

![Dockerfile Example](assets/docker_file_example.png)

---

## Building Problem

**Problem**: A common problem in projects with `Docker` is when we change our source code, all the instructions of `Dockerfile` after `COPY` command will re run (such as `npm install`)

**Answer**: Answer is simple :), exclude the sources `COPY` from `package.json` `COPY` :)

![Dockerfile Problem](assets/docker_file_problem.png)

Now after changing sources, only the instructions after `RUN npm install` will re run.

---

## Advance Commands

### FROM

Specifies the `Base Image` of our image

```dockerfile
FROM {user}/{image}:{version} as {stage}

FROM node:alpine

FROM node:alpine as builder
```

---

### MAINTAINER

Specifies the information about creator

```dockerfile
MAINTAINER {name} {email}

MAINTAINER KoLiBer koliberr136a1@gmail.com
```

---

### WORKDIR

Changes the active directory in the image we are building

```dockerfile
WORKDIR {path}

WORKDIR /usr/myApp
```

---

### ADD - COPY

Both will copy some files, folders, **url** from `Our HDD` to `Image HDD`

> `ADD` commands also supports `URLs` (new version)

```dockerfile
ADD --from={stage} {our_path} {image_path}

WORKDIR /usr/myApp
ADD ./package.json ./
RUN npm install
ADD ./ ./

COPY --from=builder ./ ./
```

---

### RUN - CMD - ENTRYPOINT

1. `RUN` => run command in image (`Add Layer`)
2. `CMD` => default command of image - overridable with `docker run -c ...` (`Default Command` - `Overridable`)
3. `ENTRYPOINT` => default command of image - not overridable (`Default Command` - `Not Overridable`)

```dockerfile
RUN {command}
CMD ["{command}", "{param1}", "{param2}", ...]
ENTRYPOINT ["{command}", "{param1}", "{param2}", ...]

RUN apk add --update redis
CMD ["redis-server"]
ENTRYPOINT ["redis-server"]
```

---

### ENV

Defines an `env` variable

```dockerfile
ENV {key} {value}

ENV name KoLiBer
RUN echo $name
```

---

### EXPOSE

Signs a container port for external usage

> on public providers like `AWS`, `GCP`, etc, the provider must be aware of our container exposed port, so in production use we must use `EXPOSE` command

```dockerfile
EXPOSE {port}

EXPOSE 8080
```

---
